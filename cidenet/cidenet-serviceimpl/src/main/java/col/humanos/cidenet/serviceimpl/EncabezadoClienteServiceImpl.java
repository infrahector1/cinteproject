package col.humanos.cidenet.serviceimpl;

import col.humanos.cidenet.EncabezadoClienteRepository;
import col.humanos.cidenet.EncabezadoClienteService;
import col.humanos.cidenet.model.EncabezadoCliente;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EncabezadoClienteServiceImpl implements EncabezadoClienteService {
  
  /** The Constant LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EncabezadoClienteServiceImpl.class);

  private EncabezadoClienteRepository encabezadoClienteRepository;

  @Autowired
  public EncabezadoClienteServiceImpl(EncabezadoClienteRepository encabezadoClienteRepository) {
    this.encabezadoClienteRepository=encabezadoClienteRepository;
  }


  @Override
  public EncabezadoCliente create(String linea,EncabezadoCliente encabezadoCliente) {
    configurarEncabezado(linea,encabezadoCliente);
    return encabezadoClienteRepository.save(encabezadoCliente);
  }

  private EncabezadoCliente configurarEncabezado(String linea,EncabezadoCliente encabezadoCliente){
    encabezadoCliente.setModalidadPlanilla(StringUtils.substring(linea,2,3));
    encabezadoCliente.setSecuencia(StringUtils.substring(linea,3,7));
    encabezadoCliente.setNombreRazonSocial(StringUtils.substring(linea,7,207).trim());
    encabezadoCliente.setTipoDocumento(StringUtils.substring(linea,207,209).trim());
    encabezadoCliente.setNroIdentificacionAportantes(StringUtils.substring(linea,209,225).trim());
    encabezadoCliente.setDigitoVerificacion(StringUtils.substring(linea,225).trim());

    return encabezadoCliente;
  }

}
