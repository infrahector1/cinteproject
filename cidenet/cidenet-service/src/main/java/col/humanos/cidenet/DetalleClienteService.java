package col.humanos.cidenet;

import col.humanos.cidenet.model.DetalleCliente;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface DetalleClienteService {

  @Transactional
  DetalleCliente create(String linea, DetalleCliente detalleCliente);

  List<DetalleCliente> busquedaAvanzada();


}
